Un simple "processeur" de pile

Associe à une pile un type de traitement, et l'execute sur cette pile manuellement,
ou automatiquement via une stratégie.

Exemple de traitement :
- envoi de notification, d'emails
- indexation, mise en cache....
- ...

Exemple de stratégies :
- un traitement tous les 10 éléments
- un traitement si X secondes écoulées depuis le dernier traitement
- un traitement lorsque le script PHP s'arrete
- ...

Exemples de cas d'utilisation :
- différer l'envoi de notification après l'éxécution d'une page web par exemple (email ou autre)
- différer l'indexation d'éléments