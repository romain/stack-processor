<?php
namespace Romain\StackProcessor;

use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Strategy;


/**
 * Multi stack processor manager trait
 * Each stack is identified by a name,
 * and associated to a strategy and a processor
 * 
 *
 * Whithout strategy, the main Strategy\Basic will be used (doing nothing automatically)
 * Without processor, the Processor\Nothing will be used (doing nothin at all)
 *
 */
trait ManagerMultiTrait {

	/**
	 * 
	 * @var array stacks and their strategy and processor
	 */
	protected $_stacks = array();
	
	/**
	 * Set a stack
	 * @param string $name
	 * @param \Romain\StackProcessor\Stack\Base $stack
	 * @param \Romain\StackProcessor\Processor\ProcessorInterface $processor
	 * @param \Romain\StackProcessor\Strategy\StrategyInterface $strategy
	 * @throws \Exception
	 */
	public function setStack($name, Stack\Base $stack, Processor\ProcessorInterface $processor = null, Strategy\StrategyInterface $strategy = null) {
		if(empty($stack)) {
			throw new \Exception('Stack can not be null');
		}
		// if no strategy given, use the main one
		if(empty($strategy)) {
			$strategy = new Strategy\Base();
		}
		// if no process given, use the main one
		if(empty($processor)) {
			$processor = new Processor\Nothing();
		}
		
		$this->_stacks[(string)$name] = array(
			'stack' => $stack,
			'processor' => $processor,
			'strategy' => $strategy
		);
	}
	
	/**
	 * Unset a stack
	 * @param string $name
	 */
	public function unsetStack($name) {
		unset($this->_stacks[$name]);
	}
	
	/**
	 * Unset all stacks
	 */
	public function unsetAll() {
		$this->_stacks = array();
	}
	
	/**
	 * Process a stack
	 * @param type $name
	 */
	public function process($name) {
		$stack = $this->_stack($name);
		$stack['strategy']->processAll($stack['stack'], $stack['processor']);
	}
	
	/**
	 * Process all stacks
	 */
	public function processAll() {
		foreach($this->_stacks as $stack) {
			$stack['strategy']->processAll($stack['stack'], $stack['processor']);
		}
	}
	
	/**
	 * Add one element to a stack
	 * @param type $element
	 */
	public function push($name, $element) {
		$stack = $this->_stack($name);
		$stack['strategy']->push($element, $stack['stack'], $stack['processor']);
	}
	
	/**
	 * Add many elements to the stack
	 * @param array|\Traversable $elements
	 */
	public function pushMany($name, $elements) {
		$stack = $this->_stack($name);
		
		// check elements is Traversable or array
		if(is_array($elements)) {
			$elements = new \ArrayIterator($elements);
		} else if(!($elements instanceof \Traversable)) {
			throw new \Exception('elements is not \Traversable or not an array');
		}
		
		$stack['strategy']->pushMany($elements, $stack['stack'], $stack['processor']);
	}
	
	/**
	 * Clear a stack
	 * @param string $name
	 */
	public function clear($name) {
		$stack = $this->_stack($name);
		$stack['strategy']->clear($stack['stack'], $stack['processor']);
	}
	
	/**
	 * Clear all stacks
	 */
	public function clearAll() {
		foreach($this->_stacks as $stack) {
			$stack['strategy']->clear($stack['stack'], $stack['processor']);
		}
	}
	
	/**
	 * Get a stack by name
	 * @param string $name
	 * @return array
	 * @throws Exception
	 */
	protected function _stack($name) {
		if(!isset($this->_stacks[$name])) {
			throw new \Exception('Stack not found : '.(string)$name);
		}
		return $this->_stacks[$name];
	}
}