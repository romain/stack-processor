<?php
namespace Romain\StackProcessor\Strategy;

use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Stack;


/**
 * The main simple basic strategy, doing nothing automatically
 */
class Base implements StrategyInterface {
	
	public function pushMany(\Traversable $elements, Stack\Base $stack, Processor\ProcessorInterface $process) {
		return $stack->pushMany($elements);
	}

	public function push($element, Stack\Base $stack, Processor\ProcessorInterface $process) {
		return $stack->push($element);
	}

	public function clear(Stack\Base $stack, Processor\ProcessorInterface $process) {
		return $stack->clear();
	}

	public function processAll(Stack\Base $stack, Processor\ProcessorInterface $process) {
		$return = $process->processAll($stack->getIterator());
		$stack->clear();
		return $return;
	}

}