<?php
namespace Romain\StackProcessor\Strategy;

use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Stack;

/**
 * Minimum limit strategy :
 * when limit reached, processAll is called
 */
class Mininum extends Base {
	
	/**
	 * Stack limit
	 * @var int
	 */
	protected $_limit = null;
	
	/**
	 * 
	 * @param int $limit
	 */
	public function __construct($limit) {
		$this->_limit = (int)$limit;
	}

	public function pushMany(\Traversable $elements, Stack\Base $stack, Processor\ProcessorInterface $process) {
		foreach($elements as $element) {
			$this->push($element, $stack, $process);
		}
	}

	public function push($element, Stack\Base $stack, Processor\ProcessorInterface $process) {
		$stack->push($element);
		if($stack->count() >= $this->_limit) {
			$process->processAll($stack->getIterator());
			$stack->clear();
		}
	}
}