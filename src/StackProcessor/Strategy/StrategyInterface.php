<?php
namespace Romain\StackProcessor\Strategy;

use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Stack;

/**
 * Une strategie de stack est capable de déclencher des traitements
 * automatiquement
 *
 * strategy($strategy) : set la strategie à utiliser pour auto déclencher les traitements
 * traitement($traitement) : object capable de gérer un traitement
 * addOne($element)
 * addMany($elements)
 * clear()
 *
 */
interface StrategyInterface {

	/**
	 * Process all the stack
	 */
	public function processAll(Stack\Base $stack, Processor\ProcessorInterface $process);

	/**
	 * Add one element
	 * @param type $element
	 */
	public function push($element, Stack\Base $stack, Processor\ProcessorInterface $process);

	/**
	 * Add many elements
	 * @param array $elements
	 */
	public function pushMany(\Traversable $elements, Stack\Base $stack, Processor\ProcessorInterface $process);

	/**
	 * Clear the pull
	 */
	public function clear(Stack\Base $stack, Processor\ProcessorInterface $process);
}