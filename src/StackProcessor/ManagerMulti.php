<?php
namespace Romain\StackProcessor;

/**
 * Multi stack manager using ManagerMultiTraitP
 */
class ManagerMulti {
	use ManagerMultiTrait;
}