<?php
namespace Romain\StackProcessor\Stack;

/**
 * Abstract stack class
 * with some basic methods implementation
 *
 * Concret classes *MUST* implements \Traversable (Iterator, IteratorAggregate...)
 *
 */
abstract class Base implements StackInterface, \Countable/*, \Traversable*/ {

	/**
	 *
	 * @param type $elements
	 * @throws Exception
	 */
	public function __construct(\Traversable $elements = null) {
		if(!($this instanceof \Traversable)) {
			throw new \Exception(get_called_class().' classes MUST implements \Traversable (Iterator, IteratorAggregate...)');
		}

		if(!empty($elements)) {
			$this->pushMany($elements);
		}
	}

	/**
	 * Basic isEmpty() implementation
	 * @return boolean
	 */
	public function isEmpty() {
		foreach($this as $element) {
			return false;
		}
		return true;
	}

	/**
	 * Basic all() implementation
	 * @return array
	 */
	public function all() {
		$all = array();
		foreach($this as $element) {
			$all[] = $element;
		}
		return $all;
	}
}