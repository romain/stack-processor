<?php
namespace Romain\StackProcessor\Stack;


/**
 * Stack interface
 *
 */
interface StackInterface {

	/**
	 * Push on element
	 * @param mixed $element
	 */
	public function push($element);

	/**
	 * Push many elements
	 * @param Traversable $elements
	 */
	public function pushMany(\Traversable $elements);

	/**
	 * Return all stack elements in an array
	 * @return array
	 */
	public function all();

	/**
	 * Clear the stack
	 */
	public function clear();
	
	/**
	 * Is the stack empty
	 * @return boolean
	 */
	public function isEmpty();
	
}