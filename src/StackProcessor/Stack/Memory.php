<?php
namespace Romain\StackProcessor\Stack;

use Romain\StackProcessor\Stack;

/**
 * Memory stack (array)
 *
 */
class Memory extends Stack\Base implements \IteratorAggregate {

	/**
	 * Stack in memory (array)
	 * @var array 
	 */
	protected $_stack_elements = array();

	/**
	 * Retourne un iterateur sur la pile
	 * @return \Iterator
	 */
	public function getIterator() {
		return new \ArrayIterator($this->_stack_elements);
	}

	/**
	 * Return all the stack
	 * @return \Traversable
	 */
	public function all() {
		return $this->_stack_elements;
	}

	/**
	 * Vide le pull
	 */
	public function clear() {
		$this->_stack_elements = array();
	}

	public function count($mode = 'COUNT_NORMAL') {
		return count($this->_stack_elements);
	}

	public function push($element) {
		array_push($this->_stack_elements, $element);
	}

	public function pushMany(\Traversable $elements) {
		foreach($elements as $element) {
			$this->push($element);
		}
	}

}