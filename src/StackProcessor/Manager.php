<?php
namespace Romain\StackProcessor;

use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Strategy;


/**
 * Simple stack manager using ManagerTrait
 */
class Manager {
	use ManagerTrait;

	/**
	 *
	 * @param Stack\StackInterface $stack
	 * @param Processor\ProcessorInterface $process
	 * @param Strategy\StrategyInterface $strategy if no strategy set, main Strategy\Basic will be used
	 */
	public function __construct(Stack\Base $stack, Processor\ProcessorInterface $process = null, Strategy\StrategyInterface $strategy = null) {
		$this->setStack($stack);
		$this->setProcess($process);
		$this->setStrategy($strategy);
	}
}