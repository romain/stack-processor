<?php
namespace Romain\StackProcessor;

use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;
use Romain\StackProcessor\Strategy;


/**
 * Simple stack processor manager trait
 *
 * Whithout strategy, the main Strategy\Basic will be used (doing nothing automatically)
 * Without processor, the Processor\Nothing will be used (doing nothin at all)
 *
 */
trait ManagerTrait {

	/**
	 * 
	 * @var Stack\StackInterface
	 */
	protected $_stack = null;

	/**
	 *
	 * @var Strategy\StrategyInterface
	 */
	protected $_strategy = null;

	/**
	 *
	 * @var Processor\ProcessorInterface
	 */
	protected $_proccess = null;
	
	/**
	 *
	 * @param Stack\StackInterface $stack
	 * @param Processor\ProcessorInterface $process
	 * @param Strategy\StrategyInterface $strategy if no strategy set, main Strategy\Basic will be used
	 */
	public function __construct(Stack\Base $stack, Processor\ProcessorInterface $process = null, Strategy\StrategyInterface $strategy = null) {
		$this->setStack($stack);
		$this->setProcess($process);
		$this->setStrategy($strategy);
	}
	
	/**
	 * Set a new Stack
	 * @param \Romain\StackProcessor\Stack\Base $stack
	 * @throws \Exception
	 */
	public function setStack(Stack\Base $stack) {
		if(empty($stack)) {
			throw new \Exception('Stack can not be null');
		}
		$this->_stack = $stack;
	}

	/**
	 * Set a new strategy
	 * @param Strategy\StrategyInterface $strategy
	 */
	public function setStrategy(Strategy\StrategyInterface $strategy) {
		// if no strategy given, use the main one
		if(empty($strategy)) {
			$strategy = new Strategy\Base();
		}
		$this->_strategy = $strategy;
	}

	/**
	 * Set a new processor
	 * @param Processor\ProcessorInterface $process
	 */
	public function setProcess(Processor\ProcessorInterface $process) {
		// if no process given, use the main one
		if(empty($process)) {
			$process = new Processor\Nothing();
		}
		$this->_proccess = $process;
	}

	/**
	 * Process the stack
	 */
	public function process() {
		$this->_strategy->processAll($this->_stack, $this->_proccess);
	}

	/**
	 * Add one element to the stack
	 * @param type $element
	 */
	public function push($element) {
		$this->_strategy->push($element, $this->_stack, $this->_proccess);
	}

	/**
	 * Add many elements to the stack
	 * @param array|\Traversable $elements
	 */
	public function pushMany($elements) {
		// check elements is Traversable or array
		if(is_array($elements)) {
			$elements = new \ArrayIterator($elements);
		} else if(!($elements instanceof \Traversable)) {
			throw new \Exception('elements is not \Traversable or not an array');
		}
		$this->_strategy->pushMany($elements, $this->_stack, $this->_proccess);
	}

	/**
	 * Clear the stack
	 */
	public function clear() {
		$this->_strategy->clear($this->_stack, $this->_proccess);
	}
}