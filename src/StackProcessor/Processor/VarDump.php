<?php
namespace Romain\StackProcessor\Processor;

/**
 * Dummy stack processor that var_dump() the stack
 *
 */
class VarDump implements ProcessorInterface {

	/**
	 * Process a stack
	 * @param mixed $element
	 */
	public function processOne($element) {
		var_dump('processOne');
		var_dump($element);
		var_dump('processOne done');
	}
	
	/**
	 * 
	 * @param Iterator $iterator
	 */
	public function processAll(\Traversable $iterator) {
		var_dump('processAll');
		$all = array();
		foreach($iterator as $element) {
			$all[] = $element;
		}
		var_dump($all);
		var_dump('processAll done');
	}
	
}