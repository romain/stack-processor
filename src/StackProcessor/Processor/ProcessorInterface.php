<?php
namespace Romain\StackProcessor\Processor;

/**
 * Stack processor interface
 *
 */
interface ProcessorInterface {

	/**
	 * Process an element
	 * @param mixed $element
	 */
	public function processOne($element);
	
	/**
	 * Process elements
	 * @param Iterator $iterator
	 */
	public function processAll(\Traversable $iterator);
	
}