<?php
namespace Romain\StackProcessor\Processor;

/**
 * Dummy stack processor that var_dump() the stack
 *
 */
class Nothing implements ProcessorInterface {

	public function processOne($element) {}
	
	public function processAll(\Traversable $iterator) {}

}