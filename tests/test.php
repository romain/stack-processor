<?php

require_once '../vendors/autoload.php';

use Romain\StackProcessor;
use Romain\StackProcessor\Strategy;
use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;

$stack = new Stack\Memory;
$processor = new Processor\VarDump;
$strategy = new Strategy\Mininum(2);

$manager = new StackProcessor\Manager($stack, $processor, $strategy);


$manager->push('coucou');
$manager->push(array('bla' => 'bli'));
$manager->pushMany(new ArrayIterator(array('un', 'lgkdjfgkldjfgkldjg')));
$manager->pushMany(array('sdlkjsdlkfj', 'AHHHHHHHHHH'));
$manager->process();
$manager->process();