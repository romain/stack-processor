<?php
namespace Romain\StackProcessor\Tests\Stack;

require_once 'vendors/autoload.php';

use Romain\StackProcessor\Stack;

class MemoryTest extends \PHPUnit_Framework_TestCase {

	public function testBase() {
		$stack = new Stack\Memory();
		
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
		
		$stack->push('coucou');
		
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('coucou'), $stack->all());
		$this->assertEquals(1, $stack->count());
		
		$stack->clear();
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
		
		
		$stack->push('coucou');
		$stack->push('bonjour');
		
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('coucou', 'bonjour'), $stack->all());
		$this->assertEquals(2, $stack->count());
		
		$stack->pushMany(new \ArrayIterator(array('1', '2')));
		
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('coucou', 'bonjour', '1', '2'), $stack->all());
		$this->assertEquals(4, $stack->count());
		
		$all = array('coucou', 'bonjour', '1', '2');
		foreach($stack as $element) {
			$this->assertEquals(array_shift($all), $element);
		}
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('coucou', 'bonjour', '1', '2'), $stack->all());
		$this->assertEquals(4, $stack->count());
		
		
		$stack->clear();
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
	}
}