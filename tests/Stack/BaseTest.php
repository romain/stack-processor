<?php
namespace Romain\StackProcessor\Tests\Stack;

require_once 'vendors/autoload.php';

class BaseTest extends \PHPUnit_Framework_TestCase {

	public function testBase() {

		// test contructor
		try {
			$stack = new StackWrongTest();
			$this->assertTrue(false);
		} catch (\Exception $ex) {
			$this->assertTrue(true);
		}
		try {
			$stack = new StackOkTest();
			$this->assertTrue(true);
		} catch (\Exception $ex) {
			$this->assertTrue(false);
		}
		
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
	}
	
	public function testStack() {
		
/*
		$year = 2014;
		$month = 4;
		$day = 30;

		$analyzer = $this->getMockBuilder('Vtech\Delorean\Analyzer\Analyzer')->disableOriginalConstructor()->getMock();
		$analyzer->expects($this->any())->method('analyze')->will($this->returnValueMap([
			['year', new Year($year)],
			['month', new MonthYear($month, $year)],
			['day', new DayMonthYear($day, $month, $year)],
		]));

		$delorean = new Delorean($analyzer);

		$this->assertEquals('20140101/20141231', $delorean->normalize('year', 'Ymd'));
		$this->assertEquals('20140401/20140430', $delorean->normalize('month', 'Ymd'));
		$this->assertEquals('20140430/20140430', $delorean->normalize('day', 'Ymd'));


		$formats = [
			Date::PRECISION_YEAR => 'Y',
			Date::PRECISION_MONTH => 'Ym',
			Date::PRECISION_DAY => 'Ymd',
		];

		$this->assertEquals('2014/2014', $delorean->normalize('year', $formats));
		$this->assertEquals('201404/201404', $delorean->normalize('month', $formats));
		$this->assertEquals('20140430/20140430', $delorean->normalize('day', $formats));
 * 
 */
	}
}




class StackOkTest extends \Romain\StackProcessor\Stack\Base implements \IteratorAggregate {
	
	protected $_stack = array();
	
	public function clear() {}
	public function count($mode = 'COUNT_NORMAL') {}
	public function push($element) {}
	public function pushMany(\Traversable $elements) {}

	public function getIterator() {
		return new \ArrayIterator($this->_stack);
	}

}

class StackWrongTest extends \Romain\StackProcessor\Stack\Base {
	public function clear() {}
	public function count($mode = 'COUNT_NORMAL') {}
	public function push($element) {}
	public function pushMany(\Traversable $elements) {}
}