<?php
namespace Romain\StackProcessor\Tests\Strategy;

require_once 'vendors/autoload.php';

use Romain\StackProcessor\Strategy;
use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;

class BaseTest extends \PHPUnit_Framework_TestCase {

	public function testBase() {
		$stack = new Stack\Memory();
		$processor = new Processor\Nothing();
		$strategy = new Strategy\Base();
		
		$strategy->pushMany(new \ArrayIterator(array('1', '2', '3')), $stack, $processor);
		
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('1', '2', '3'), $stack->all());
		$this->assertEquals(3, $stack->count());
		
		$strategy->push('4', $stack, $processor);
		
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('1', '2', '3', '4'), $stack->all());
		$this->assertEquals(4, $stack->count());
		
		$strategy->clear($stack, $processor);
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
	}
}