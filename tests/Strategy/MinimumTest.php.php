<?php
namespace Romain\StackProcessor\Tests\Strategy;

require_once 'vendors/autoload.php';

use Romain\StackProcessor\Strategy;
use Romain\StackProcessor\Stack;
use Romain\StackProcessor\Processor;

class MinimumTest extends \PHPUnit_Framework_TestCase {

	public function testBase() {
		$stack = new Stack\Memory();
		$process = new Processor\Nothing();
		$strategy = new Strategy\Mininum(2);
		
		$strategy->push('1', $stack, $process);
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('1'), $stack->all());
		$this->assertEquals(1, $stack->count());
		
		$strategy->push('2', $stack, $process);
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
		
		$strategy->push('1', $stack, $process);
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('1'), $stack->all());
		$this->assertEquals(1, $stack->count());
		
		$strategy->pushMany(new \ArrayIterator(array('2')), $stack, $process);
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
		
		$strategy->pushMany(new \ArrayIterator(array('1', '2')), $stack, $process);
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
		
		$strategy->pushMany(new \ArrayIterator(array('1', '2', '3')), $stack, $process);
		$this->assertFalse($stack->isEmpty());
		$this->assertEquals(array('3'), $stack->all());
		$this->assertEquals(1, $stack->count());
		
		$strategy->clear($stack, $process);
		$this->assertTrue($stack->isEmpty());
		$this->assertEquals(array(), $stack->all());
		$this->assertEquals(0, $stack->count());
	}
}